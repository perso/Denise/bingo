#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Configuration générale
"""

# Configuration par défaut
DEFAUT = {}

DEFAUT["titre"] = "Le bingo de l'allaitement"

DEFAUT["nblignes"] = 4
DEFAUT["nbcolonnes"] = 3
DEFAUT["nbcasesvides"] = 0


# Configuration

CONFIG = {}
CONFIG["lmax_titre"] = 50
CONFIG["maxlignes"] = 20
CONFIG["minlignes"] = 1
CONFIG["maxcolonnes"] = 20
CONFIG["mincolonnes"] = 1