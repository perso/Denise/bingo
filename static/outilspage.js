/* Calcul des scores */
var points_par_case = 1 ;
var points_par_ligne = 10 ;



// Couleurs des cases
var couleur_base = "rgb(238, 238, 238)" ;
var couleur_valide = 'rgb(255, 136, 136)' ;


// Valide la case et la colore en rouge
function validecase(elem) {
	elem.style.backgroundColor = couleur_valide ;
	
	detectelignes() ;
}

// efface la grille
function effacegrille() {
	var table=document.getElementById("grille") ;
	var listetd = table.getElementsByTagName("td") ;
	for (var i=0; i< listetd.length; i++){
		listetd[i].style.backgroundColor = couleur_base ;
	}
	metscore(0) ;
}

// mettre à jour le score si on veut
function metscore(score) {
	document.getElementById("score").innerHTML = score ;
}

// Compte les lignes, colonnes et diagonales de la grille et met à jour le score
function detectelignes(elem) {
	var table=document.getElementById("grille") ;
	var listelignes = table.getElementsByTagName("tr") ;
	var nblignes = listelignes.length ;
	var nbcolonnes = listelignes[0].children.length ;
	var i, j ;
	
	// lignes
	var nb_lignes_completes = 0 ;
	var remplie ;
	for(i=0; i<nblignes; i++) {
		remplie = true ;
		for(j=0; j<nbcolonnes; j++) {
			//alert(j) ;
			//alert(listelignes[i].children[j].style.backgroundColor) ;
			if(listelignes[i].children[j].style.backgroundColor != couleur_valide)	{
				// c'est pas une ligne, dommage
				remplie = false ;
				//alert("la ligne "+i+"n'est pas remplie, vu à la colonne "+j) ;
				break
			}
		}
		if(remplie) {
			nb_lignes_completes+=1 ;
		}
	}
	
	// Colonnes
	var nb_colonnes_completes = 0 ;
	for(j=0; j<nbcolonnes; j++) {
		remplie = true ;
		for (i=0; i<nblignes; i++) {
			if(listelignes[i].children[j].style.backgroundColor != couleur_valide)	{
				// c'est pas une colonne, dommage
				remplie = false ;
				break
			}
		}
		if (remplie) {
			nb_colonnes_completes+=1 ;
		}
	}

	// Diagonales.
	var nb_diagonales_1 = 0 ;
	var nb_diagonales_2 = 0 ;
	if(nblignes <= nbcolonnes) {
		// Les diagonales se comptent horizontalement : "\\\" et "///"
		for(i=0; i<nbcolonnes - nblignes +1; i++) {
			// diagonales "\"
			remplie=true ;
			for(j=0; j<nblignes; j++) {
				if(listelignes[j].children[i+j].style.backgroundColor != couleur_valide)	{
					remplie = false
					break
				}
			}
			if (remplie) {
				nb_diagonales_1+=1
			}
			
			// diagonales "/"
			remplie=true ;
			for(j=0; j<nblignes; j++) {
				if(listelignes[j].children[nbcolonnes-i-j-1].style.backgroundColor != couleur_valide)	{
					remplie = false
					break
				}
			}
			if (remplie) {
				nb_diagonales_2+=1
			}
		}
		
	} else { // les diagonales vont se compter "verticalement"
		for(i=0; i<nblignes - nbcolonnes +1; i++) {
			// diagonales "\"
			remplie=true ;
			for(j=0; j<nbcolonnes; j++) {
				if(listelignes[i+j].children[j].style.backgroundColor != couleur_valide)	{
					remplie = false
					break
				}
			}
			if (remplie) {
				nb_diagonales_1+=1
			}
			
			// diagonales "/"
			remplie=true ;
			for(j=0; j<nbcolonnes; j++) {
				if(listelignes[nblignes -i-j-1].children[j].style.backgroundColor != couleur_valide)	{
					remplie = false
					break
				}
			}
			if (remplie) {
				nb_diagonales_2+=1
			}
		}
		
		
	}
	// Compter les cases utilisées
	var nbcases = 0 ;
	for(i=0; i<nblignes; i++) {
		for(j=0; j<nbcolonnes; j++) {
			if (listelignes[i].children[j].style.backgroundColor == couleur_valide) {
				nbcases+=1
			}
		}
		
	}
	
	
	// Mettre le score à jour
	score = (nb_lignes_completes + nb_colonnes_completes + nb_diagonales_1 + nb_diagonales_2) *points_par_ligne + nbcases ;
	metscore(score) ;
}
