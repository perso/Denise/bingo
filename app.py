#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 26 17:31:48 2021

@author: sekhmet
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import flask
from data_textes import liste_textes
import gere_grille as g
import gere_erreurs as e
from config import DEFAUT, CONFIG


def initialise_mode_beta():
    global beta
    hote = flask.request.host
    if hote[:4] == "beta":
        print("** Mode bêta !**")
        return True
    else:
        return False


app = flask.Flask(__name__)


@app.route('/', methods=["GET", "POST"])
def index():
    liste_err = e.initialise_erreurs()
    idg = flask.request.args.get("grille", "") # Id de grille passée en param (ou pas)
    if idg != "": # Si on a mis un url de grille
        conf = g.decode_grille(idg, liste_err)
        if conf == {}: # Erreur à la génération
             bingo = g.genere_grille(DEFAUT, liste_textes)
             conf = DEFAUT.copy()
             e.erreur("L'url de la grille n'est pas valide...", liste_err)
        else:
            bingo=conf["grille"]
    else:
        # On récupère les données post (et ça sera défaut si y'a rien)
        if flask.request.method == "POST":
            conf = g.gere_donnees_custom(flask.request.form, liste_err)
        else:
            conf= DEFAUT.copy()
        bingo = g.genere_grille(conf, liste_textes) # aléatoire
        
    chainecode = g.encode_grille(conf, bingo, liste_err)
    
    return flask.render_template("index.html", bingo=bingo, chainecode=chainecode, conf=conf, e=liste_err[0]+liste_err[1]+liste_err[2])
 
    
@app.route('/custom')
def custom():
    liste_err = e.initialise_erreurs()
    return flask.render_template("custom.html", DEFAUT=DEFAUT,CONFIG=CONFIG, e=liste_err[0]+liste_err[1]+liste_err[2])

if __name__ == "__main__":
 #   print("Mode debug maison : "+str(niveau_debug))
    app.run(host='0.0.0.0',debug=True)