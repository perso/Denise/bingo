#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 26 22:50:08 2021

@author: sekhmet
"""

liste_textes = [
        "Tu vas pas l'allaiter jusqu'à [âge quelconque] !",
        "Tu es sûre que tu as assez de lait ?",
        "Quand est-ce que tu vas lui donner du lait normal ?",
        "Il/elle fait ses nuits ?",
        "Il/elle dort encore avec vous ?",
        "Laisse le/la pleurer il/elle finira bien par dormir (et/ou ça lui fera les poumons).",
        "Ton lait n'est pas/plus assez nourrissant.",
        "Il/elle doit apprendre à se détacher de maman.",
        "Tu as pris combien de kilos ?",
        "Donne lui un biberon, il/elle fera ses nuits.",
        "Tu arrêteras quand il/elle aura des dents, puisqu'il/elle va te mordre",
        
        ]