#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 26 22:58:28 2021

@author: sekhmet
"""
import random
import zlib, base64, json
import gere_erreurs as e
from config import DEFAUT, CONFIG

def genere_grille(config, textes):
    """ génère une grille avec des éléments aléatoires du tableau textes. Si y'a pas assez, on complète aléatoirement avec des 0.
    config contient tout ce qu'on veut pour la grille"""
    
    nblignes, nbcol = config["nblignes"], config["nbcolonnes"]
    
    # Créer la grille
    grille = [ [""]*nbcol for i in range(nblignes)]
    
    nbdata = nbcol*nblignes - config["nbcasesvides"] # Nombre d'entrées
    
    # Créer le sous-tableau
    if len(textes)>= nbdata: # Si y'a assez de textes
        soustab = random.sample(textes, nbdata) + ["0"]*config["nbcasesvides"]
    else:
        # On met tous les textes + complète avec des "0"
        soustab = textes + ["0"]*(nbcol*nblignes - len(textes))
    random.shuffle(soustab)
    #print(soustab)
    
    # On met tout ça dans la grille
    for i in range(nblignes):
        for j in range(nbcol):
            grille[i][j] = soustab[i*nbcol+j]
            
    return grille

def encode_grille(config, grille, liste_err):
    """ grille est un tableau double qu'on veut sauvegarder.
    config est la configuration (on va garder le titre avec)
    Renvoie une chaîne de caractères qui encode cette grille
    de manière condensée. Renvoie une chaîne cheloue"""
    # Chaîne de caractère
    data = {"titre": config["titre"], "grille": grille}
    chaine = json.dumps(data)
    code = zlib.compress(chaine.encode())
    chaineencodee = str(base64.urlsafe_b64encode(code))
    e.debug(chaineencodee, liste_err)
    return chaineencodee[2:-1] # Enlever le b' devant et le ' à la fin

def decode_grille(chaineencodee, liste_err):
    """ l'inverse de la fonction précédente : renvoie le dictionnaire
    avec les params et la grille reconstituée
    """
    e.debug(chaineencodee, liste_err)
    b2 = base64.urlsafe_b64decode(chaineencodee)
    try:
        decodee = zlib.decompress(b2)
    except:
        e.erreur("Impossible de décoder la chaîne : "+chaineencodee, liste_err)
        return {}
    # Remettre ça en python
    data = json.loads(decodee)
    return data

###########################

def gere_donnees_custom(data, liste_err):
    """ data est le dictionnaire de requête. Gère les données reçues
    et en fait un dictionnaire propre, en mettant les défauts là où
    c'est pas bon"""
    
    conf =DEFAUT.copy()
    
    ## Le titre
    t = data.get("titre", "")
    if len(t) > 0:
        conf["titre"] = t[0:CONFIG["lmax_titre"]]
        
    # Les dimensions
    conf["nblignes"] = minimaxi(data.get("nblignes",""), CONFIG["minlignes"], CONFIG["maxlignes"], DEFAUT["nblignes"], liste_err)
    conf["nbcolonnes"] = minimaxi(data.get("nbcolonnes",""), CONFIG["mincolonnes"], CONFIG["maxcolonnes"], DEFAUT["nbcolonnes"], liste_err)
    
    # Les cases vides
    conf["nbcasesvides"] = minimaxi(data.get("nbcasesvides",""), 0, CONFIG["maxlignes"]*CONFIG["maxcolonnes"], DEFAUT["nbcasesvides"], liste_err)
    
    return conf
###
def minimaxi(donnee, mini, maxi, defaut, liste_err):
    """ donnee est une chaine qui est censée être un nombre entier.
    On vérifie que la donnée est valide, qu'elle est bien dans l'inter
    valle mini, maxi et si ça foire on met le défaut"""
    try:
        x = int(donnee)
    except:
        e.warning("La donnée "+donnee+ "est invalide !", liste_err)
        x = defaut
    x = max(mini, min(x, maxi))
    return x